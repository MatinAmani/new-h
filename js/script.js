var element

element = document.getElementsByClassName('lang')
element[0].addEventListener('click', () => {
    document.getElementsByClassName('lang list')[0].style.display = 'flex'
    document.getElementsByClassName('lang')[0].style.display = 'none'
})

element = document.getElementsByClassName('lang-link')
for (let i = 0; i < element.length; i++) {
    element[i].addEventListener('click', (event) => {
        let link = event.target
        let temp = document.getElementById('selected-lang').innerHTML
        document.getElementsByClassName('lang list')[0].style.display = 'none'
        document.getElementsByClassName('lang')[0].style.display = 'flex'
        document.getElementById('selected-lang').innerHTML = link.text
        link.text = temp
    })
}

element = document.getElementsByClassName('menu alt')
element[0].addEventListener('click', () => {
    document.querySelector('.aside-container').classList.toggle('show')
})

element = document.getElementsByClassName('menu-item close')
element[0].addEventListener('click', () => {
    document.querySelector('.aside-container').classList.toggle('show')
})
